module.exports = {
  collectCoverageFrom: ['./**/*.js', '!**/node_modules/**'],
  coverageReporters: ['html', 'text', 'text-summary', 'cobertura'],
  reporters: ['default', 'jest-junit'],
  testMatch: ['**/*.test.js'],
  coveragePathIgnorePatterns: [
    'src/tests/*',
    'coverage/*',
    './.eslintrc.js',
    './jest.config.js',
  ],
  coverageThreshold: {
    global: {
      branches: 0,
      functions: 0,
      lines: 25,
      statements: 25,
    },
  },
};
